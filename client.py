import grpc

from air_clothing_pb2 import InputImageBytes
from air_clothing_pb2_grpc import AirClothingMAStub

# TEST_IMG = "in.jpg"
TEST_IMG = "test/819.jpg"


def img_iter(path):
    with open(path, "rb") as f:
        image_bytes = f.read()
        for idx in range(0, len(image_bytes), 1024):

            yield InputImageBytes(
                input=image_bytes[idx:idx + 1024]
            )


if __name__ == '__main__':
    with grpc.insecure_channel("0.0.0.0:15001") as channel:
        stub = AirClothingMAStub(channel)
        result = b""
        for result_part in stub.DoAirClothing(img_iter(TEST_IMG)):
            if result_part.info == "":
                result += result_part.image
            else: print(result_part.key, result_part.info)

        with open("server_out.jpg", "wb") as f:
            f.write(result)
