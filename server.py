import argparse
import time
import grpc

from concurrent import futures

from air_clothing_pb2 import OutputImageWithInfo
from air_clothing_pb2_grpc import AirClothingMAServicer, add_AirClothingMAServicer_to_server

from runner import AirClothingRunner


def parse_args():
    parser = argparse.ArgumentParser()

    parser.add_argument('--model', type=str, default='air_clothing', help='path for trained encoder')
    parser.add_argument('--arch', type=str, default='resnet50_stn', help='arch for main model')
    parser.add_argument('--encoder_path', type=str, default='encoder-12-1170.ckpt', help='path for trained encoder')

    parser.add_argument('--vocab_path1', type=str, default='json/train_up_vocab.pkl',
                        help='path for vocabulary wrapper')
    parser.add_argument('--vocab_path2', type=str, default='clothing_vocab_accessory2.pkl',
                        help='path for vocabulary wrapper')

    # Encoder - Yolo-v3 parameters
    parser.add_argument('--confidence', type=float, default=0.5, help='Object Confidence to filter predictions')
    parser.add_argument('--nms_thresh', type=float, default=0.4, help='NMS Threshhold')
    parser.add_argument('--cfg_file', type=str, default='cfg/yolov3.cfg', help='Config file')
    parser.add_argument('--weights_file', type=str, default='yolov3.weights', help='weightsfile')
    parser.add_argument('--reso', type=str, default='416',
                        help='Input resolution of the network. Increase to increase accuracy. Decrease to increase speed')
    parser.add_argument('--scales', type=str, default='1,2,3', help='Scales to use for detection')

    # Model parameters (should be same as paramters in train.py)
    parser.add_argument('--embed_size', type=int, default=256, help='dimension of word embedding vectors')
    parser.add_argument('--hidden_size', type=int, default=512, help='dimension of lstm hidden states')
    parser.add_argument('--num_layers', type=int, default=1, help='number of layers in lstm')
    parser.add_argument('--roi_size', type=int, default=13)
    parser.add_argument("--device", default=0)
    parser.add_argument("--chunk_size", default=4096)
    parser.add_argument("--port", default=15001)
    return parser.parse_args()


class AirClothingMAServer(AirClothingMAServicer):
    def __init__(self, args):
        self.runner = AirClothingRunner(args)
        self.res_chunk_size = args.chunk_size

    def DoAirClothing(self, request_iterator, context):
        request = b""

        for request_part in request_iterator:
            request += request_part.input

        result_data_list, result_img = self.runner.run(request)
        for result_data in result_data_list:
            yield OutputImageWithInfo(
                key=result_data["key"], info=result_data["info"]
            )

        for idx in range(0, len(result_img), self.res_chunk_size):
            yield OutputImageWithInfo(
                image=result_img[idx:idx + self.res_chunk_size]
            )


if __name__ == '__main__':
    args = parse_args()
    server = grpc.server(futures.ThreadPoolExecutor(max_workers=1))

    air_clothing_server = AirClothingMAServer(args=args)
    air_clothing_servicer = add_AirClothingMAServicer_to_server(air_clothing_server, server)
    server.add_insecure_port("[::]:{}".format(args.port))
    server.start()

    print("air clothing grpc server starting at 0.0.0.0:{}".format(args.port))

    try:
        while True:
            time.sleep(60 * 60 * 24)
    except KeyboardInterrupt:
        server.stop(0)
