FROM nvcr.io/nvidia/pytorch:19.06-py3

WORKDIR /workspace

COPY . ./

RUN pip install grpcio_tools==1.22.1 && \
    python -m grpc_tools.protoc -I. --python_out=. --grpc_python_out=. air_clothing.proto

WORKDIR ./RoIAlign.pytorch/

RUN python setup.py install

WORKDIR ..

EXPOSE 15001
CMD ["python", "server.py"]
