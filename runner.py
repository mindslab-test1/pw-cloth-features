import torch
import numpy as np
import argparse
import pickle
import uuid
import os
from os import listdir, getcwd
import os.path as osp
import glob
from torchvision import transforms
import torch.backends.cudnn as cudnn
# from model import EncoderClothing, DecoderClothing
from darknet import Darknet
from PIL import Image
from util import *
import cv2
import pickle as pkl
import random
from preprocess import prep_image_bytes

import resnet50 as model_n
import resnet50_stn as model_stn
import resnet50_fc as model_fc

import sys

transform_test = transforms.Compose([
    transforms.Resize(size=(256, 128)),
    transforms.ToTensor(),
    transforms.Normalize(mean=(0.485, 0.456, 0.406), std=(0.229, 0.224, 0.225))
])

#  "winter scarf", "cane", "bag", "shoes", "hat", "face"]
# attribute categories = #6
colors_a = ["", "white", "black", "gray", "pink", "red", "green", "blue", "brown", "navy", "beige", \
            "yellow", "purple", "orange", "mixed-color"]  # 0-14
pattern_a = ["", "no-pattern", "checker", "dotted", "floral", "striped", "custom-pattern"]  # 0-6
gender_a = ["", "man", "woman"]  # 0-2
season_a = ["", "spring", "summer", "autumn", "winter"]  # 0-4
upper_t_a = ["", "shirt", "jumper", "jacket", "vest", "parka", "coat", "dress"]  # 0-7
u_sleeves_a = ["", "short-sleeves", "long-sleeves", "no-sleeves"]  # 0-3

lower_t_a = ["", "pants", "skirt"]  # 0-2
l_sleeves_a = ["", "short", "long"]  # 0-2
leg_pose_a = ["", "standing", "sitting", "lying"]  # 0-3

glasses_a = ["", "glasses"]

attribute_pool = [colors_a, pattern_a, gender_a, season_a, upper_t_a, u_sleeves_a,
                  colors_a, pattern_a, gender_a, season_a, lower_t_a, l_sleeves_a, leg_pose_a]

coco_classes = load_classes('data/coco.names')
colors = pkl.load(open("pallete2", "rb"))


class AirClothingRunner:
    def __init__(self, args):
        self.args = args
        # Image preprocessing
        transform = transforms.Compose([
            transforms.ToTensor()])

        # Load vocabulary wrapper

        # Build the models
        # CUDA = torch.cuda.is_available()

        # Device configuration
        self.device = torch.device('cuda:{:d}'.format(args.device) if torch.cuda.is_available() else 'cpu')

        self.num_classes = 80
        self.yolov3 = Darknet(args.cfg_file)
        self.yolov3.load_weights(args.weights_file)
        self.yolov3.net_info["height"] = args.reso
        self.inp_dim = int(self.yolov3.net_info["height"])
        assert self.inp_dim % 32 == 0
        assert self.inp_dim > 32
        print("yolo-v3 network successfully loaded")

        attribute_size = [15, 7, 3, 5, 8, 4, 15, 7, 3, 5, 3, 3, 4]

        self.yolov3.to(self.device)
        cudnn.benchmark = False
        cudnn.deterministic = True
        self.yolov3.eval()

        self.attribute_dim = [15, 7, 3, 5, 8, 4, 15, 7, 3, 5, 3, 3, 4]

        if args.arch == 'resnet50':
            self.model = model_n.__dict__['resnet50'](pretrained=True, num_classes=len(self.attribute_dim),
                                                      attribute_dim=self.attribute_dim)
        if args.arch == 'resnet50_fc':
            self.model = model_fc.__dict__['resnet50_fc'](pretrained=True, num_classes=len(self.attribute_dim),
                                                          attribute_dim=self.attribute_dim)
        if args.arch == 'resnet50_stn':
            self.model = model_stn.__dict__['resnet50_stn'](pretrained=True, num_classes=len(self.attribute_dim),
                                                            attribute_dim=self.attribute_dim)
        else:
            print("error setting resnet")
            exit(0)

        check_point = torch.load('checkpoints/' + args.model + '_best.pth.tar')
        state_dict = check_point['state_dict']

        from collections import OrderedDict
        new_state_dict = OrderedDict()
        for k, v in state_dict.items():
            name = k[7:]  # remove `module.`
            new_state_dict[name] = v
        self.model.load_state_dict(new_state_dict)
        self.model.to(self.device)
        self.model.eval()

    def run(self, img_bytes):
        tmp_file_name = str(uuid.uuid4())
        with open("{}.jpg".format(tmp_file_name), "wb") as f:
            f.write(img_bytes)
        # orig_pil_image = Image.frombytes('RGBA', (128, 128), img_bytes, 'raw')
        orig_pil_image = Image.open("{}.jpg".format(tmp_file_name))
        image, orig_img, im_dim = prep_image_bytes(img_bytes, self.inp_dim)

        im_dim = torch.FloatTensor(im_dim).repeat(1, 2)
        device = self.device
        image_tensor = image.to(device)
        im_dim = im_dim.to(device)

        # Generate an caption from the image
        detections = self.yolov3(image_tensor, device, True)  # prediction mode for yolo-v3
        detections = write_results(
            detections, self.args.confidence, self.num_classes, device, nms=True, nms_conf=self.args.nms_thresh)
        # original image dimension --> im_dim
        # view_image(detections)

        result_list = []

        if type(detections) != int:
            if detections.shape[0]:
                im_dim = im_dim.repeat(detections.shape[0], 1)
                scaling_factor = torch.min(self.inp_dim / im_dim, 1)[0].view(-1, 1)

                detections[:, [1, 3]] -= (self.inp_dim - scaling_factor * im_dim[:, 0].view(-1, 1)) / 2
                detections[:, [2, 4]] -= (self.inp_dim - scaling_factor * im_dim[:, 1].view(-1, 1)) / 2

                detections[:, 1:5] /= scaling_factor

                small_object_ratio = torch.FloatTensor(detections.shape[0])

                for i in range(detections.shape[0]):
                    detections[i, [1, 3]] = torch.clamp(detections[i, [1, 3]], 0.0, im_dim[i, 0])
                    detections[i, [2, 4]] = torch.clamp(detections[i, [2, 4]], 0.0, im_dim[i, 1])

                    object_area = (detections[i, 3] - detections[i, 1]) * (detections[i, 4] - detections[i, 2])
                    orig_img_area = im_dim[i, 0] * im_dim[i, 1]
                    small_object_ratio[i] = object_area / orig_img_area

                detections = detections[small_object_ratio > 0.02]
                im_dim = im_dim[small_object_ratio > 0.02]
                bboxs = detections[:, 1:5].clone()

                if detections.size(0) > 0:
                    Roi = detections.cpu().numpy().astype(int)
                    rois = []
                    for i in range(detections.shape[0]):
                        # roi = orig_img[Roi[i][2]:Roi[i][4], Roi[i][1]:Roi[i][3]]
                        roi = orig_pil_image.crop([Roi[i][1], Roi[i][2], Roi[i][3], Roi[i][4]])
                        #    roi.save(str(i)+ list_dir[inx])
                        roi = transform_test(roi).unsqueeze(0)
                        rois.append(roi)

                    rois = torch.cat(rois, 0).cuda()
                    outputs = self.model(rois)

                    # if type(outputs) == type(()) or type(outputs) == type([]):
                    if len(outputs) != 13:
                        if len(outputs) == 4:
                            outputs = [torch.max(torch.max(torch.max(outputs[0][k], outputs[1][k]),
                                                           outputs[2][k]), outputs[3][k])
                                       for k in range(len(self.attribute_dim))]
                        if len(outputs) == 5:
                            outputs = [torch.max(torch.max(torch.max(torch.max(outputs[0][k],
                                                                               outputs[1][k]), outputs[2][k]),
                                                           outputs[3][k]), outputs[4][k])
                                       for k in range(len(self.attribute_dim))]

                    print(detections.shape[0])
                    for i in range(detections.shape[0]):

                        sampled_caption = []
                        dress = False
                        # attr_fc = outputs[]
                        for j in range(len(outputs) - 1):
                            # temp = outputs[j][i].data
                            max_index = torch.max(outputs[j][i].data, 0)[1]
                            word = attribute_pool[j][max_index]
                            if word == 'dress':  # dress일 경우 처리
                                dress = True
                            if dress and j > 5:
                                dress = False
                                break
                            elif word == '' and j == 10:  # 하의의 타입이 null일 경우
                                del sampled_caption[6:]
                                break
                            elif j == 8 or j == 9:
                                continue
                            else:
                                sampled_caption.append(word)
                                if j == 5:
                                    sampled_caption.append(' // ')  # 상의 하의 분리 처리 구분 기호 넣음

                        if len(sampled_caption) > 7:
                            c10 = sampled_caption[10]
                            sampled_caption[10] = sampled_caption[9]
                            sampled_caption[9] = c10

                        sentence = ' '.join(sampled_caption)

                        # again sampling for testing
                        # print ('---------------------------')
                        print(str(i + 1) + ': ' + sentence)
                        orig_img = write(detections[i], orig_img, sentence, i + 1, coco_classes, colors)

                        # sampled_caption = []
                        # # attr_fc = outputs[]
                        # for j in range(len(outputs)):
                        #     # temp = outputs[j][i].data
                        #     max_index = torch.max(outputs[j][i].data, 0)[1]
                        #     word = attribute_pool[j][max_index]
                        #     sampled_caption.append(word)
                        #
                        # sampled_caption[10], sampled_caption[11] = sampled_caption[11], sampled_caption[10]
                        #
                        # sentence = ' '.join(sampled_caption)
                        result_data = {
                            "key": i + 1,
                            "info": sentence,
                        }
                        #
                        result_list.append(result_data)
                        # orig_img = write(detections[i], orig_img, sentence, i + 1, coco_classes, colors)

                        #
                        # # again sampling for testing
                        # # print ('---------------------------')
                        # print(str(i + 1) + ': ' + sentence)
                        # orig_img = write(detections[i], orig_img, sentence, i + 1, coco_classes, colors)
        _, result_image = cv2.imencode(".jpg", orig_img)
        os.remove("{}.jpg".format(tmp_file_name))

        return result_list, result_image.tobytes()
